//Hansen Lukman
//10-23-16
//CSE 02 - 210: Program will ask for and validate a user input of an integer from 0 to 100
//              and print out a hidden X with a size based on the input

import java.util.Scanner;
public class EncryptedX
{
    public static void main(String[] args)
    {
        Scanner scnr = new Scanner(System.in);
        int verticalAst; //integer for the top and bottom asterisk bodies
        int sideAst; //integer for the left and right asterisk bodies
        int counter = 0; //multipurpose counter
        int dimension; //integer to store dimension input
        int flip = 0; //integer to indicate if on the top or bottom half of the X
        boolean odd = true; //boolean to indicate if input value is odd or even
        String row = ""; //string to add to when building the row
        String junk = ""; //junk for scanner
        
        System.out.print("Enter an Integer from 0 to 100 for the square dimensions of your X: "); //prompts user for size of X
        while(!scnr.hasNextInt() || (dimension = scnr.nextInt()) < 0 || dimension > 100){ //checks if user input is an integer within 0 to 100
            junk = scnr.nextLine(); //clears "conveyor belt"                              //also stores user input as dimension if input is correct
            System.out.print("Please try again. Enter an Integer from 0 to 100: ");  //re-prompts user for size of X
        }
        verticalAst = dimension - 1; //sets up first row of top asterisks
        sideAst = 0; //sets up first row of left and right asterisks
        if(dimension % 2 == 0){ //if input is even
            dimension += 1; //add an extra row for empty middle space
            odd = false; //set odd boolean to false
        }
        for(int i = 0; i < dimension; i++){ //loops dimension amount of times
            if(verticalAst <= 0 && !odd){ //if at the middle white space and input is even
                while(counter < sideAst){ //loops sideAst times
                    row += "*"; //add an asterisk to the row (building left side)
                    counter++; //increments counter
                }
                row += " "; //adds the middle whitespace
                counter = 0; //resets counter
                while(counter < sideAst){ //loops sideAst times
                    row += "*"; //add an asterisk to the row (building right side)
                    counter++; //increments counter
                }
                verticalAst = 1; //sets up the bottom asterisk body for the next row
                sideAst--; //decrements sideAst for next row
                flip = 1; //indicates building bottom half
            } else if(verticalAst <= 0 && odd){ //if at the middle white space and input is even
                while(counter < sideAst){ //loops sideAst times
                    row += "*"; //add an asterisk to the row (builds left side)
                    counter++; //increments counter
                }
                row += "  "; //adds the middle whitespace 
                counter = 0; //resets counter
                while(counter < sideAst){ //loops sideAst times
                    row += "*"; //adds an asterisk to the row (builds right side)
                    counter++; //increments counter
                }
                verticalAst = 2; //sets up the bottom asterisk body for the next row
                sideAst--; //decrements sideAst for next row
                flip = 1; //indicates building bottom half
            } else if (flip == 0){ //if building the top half
                while(counter < sideAst){ //loops sideAst times
                    row += "*"; //add an asterisk to the row (builds left side)
                    counter++; //increments counter
                }
                counter = 0; //resets counter
                row += " "; //adds the space after the left asterisk body
                while(counter < verticalAst){
                    row += "*"; //add an asterisk to the row (builds top asterisk body)
                    counter++; //increments counter
                }
                counter = 0; //resets counter
                row += " "; //adds the space after the bottom asterisk body
                while(counter < sideAst){ //loops sideAst times
                    row += "*"; //adds an asterisk to the row (builds right side)
                    counter++; //increments counter
                }
                counter = 0; //resets counter
                sideAst++; //increments sideAst for the next row
                verticalAst -= 2; //decrements verticalAst by 2 for the next row
            
            } else if (flip == 1){ //if building the bottom half
                counter = sideAst; //sets counter equal to sideAst
                while(counter > 0){ //loops sideAst times using counter as sideAst
                    row += "*"; //adds an asterisk to the row (builds left side)
                    counter--; //decrements counter
                }
                row += " "; //adds the space after the left asterisk body 
                counter = verticalAst; //sets counter equal to verticalAst
                while(counter > 0){ //loops verticalAst times
                    row += "*"; //adds an asterisk to the row (builds bottom asterisk body)
                    counter--; //decrements counter
                }
                counter = sideAst; //sets counter equal to sideAst
                row += " "; //adds the space after the bottom asterisk body 
                while(counter > 0){ //loops sideAst times
                    row += "*"; //adds asterisk to the row (builds right side)
                    counter--; //decrements counter
                }
                sideAst--; //decrements sideAst for the next row
                verticalAst += 2; //decrements verticalAst by 2 for the next row
            }
            System.out.println(row); //prints the row
            row = ""; //resets the row string
        } //closes for loop
    }
}