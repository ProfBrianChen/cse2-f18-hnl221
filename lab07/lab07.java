//Hansen Lukman
//10-29-2018
//CSE 02 - 210: Program will generate random sentences and a paragraph

import java.util.Random;
import java.util.Scanner;

public class lab07
{
    public static void main(String[] args)
    {
        String fAdjective = ""; //String for finalized object in first sentence
        String fSubject = ""; //String for finalized object in first sentence
        String fVerb = ""; //String for finalized verb in first sentence
        String fObject = ""; //String for finalized object in first sentence
        String query = ""; //String for query input
        String sentenceSub = ""; //String for first sentence generated
        String sentenceAct = ""; //String for second sentence generated
        Random randomGenerator = new Random();
        Scanner scnr = new Scanner(System.in);
        do{
            int randomA = randomGenerator.nextInt(10); //generates random number for adjective
            int randomB = randomGenerator.nextInt(10); //generates random number for subject
            int randomC = randomGenerator.nextInt(10); //generates random number for verb
            int randomD = randomGenerator.nextInt(10); //generates random number for object
            fAdjective = getAdj(randomA); //returned random adjective
            fSubject = getSub(randomB); //returned random subject
            fVerb = getVerb(randomC); //returned random verb
            fObject = getObject(randomD); //returned random object
            sentenceSub = sentence(fAdjective, fSubject, fVerb, fObject); //prints the generated sentence and stores the subject
            System.out.print("Would you like a different sentence? (yes or no): "); //prompts user whether they would like a different sentence
            query = scnr.next(); //stores that user input
            while(!query.equals("yes") && !query.equals("no")){ //if the input is not a string equal to yes or no, keep prompting for the right input
                System.out.print("Invalid input, please type \"yes\" or \"no\": ");
                query = scnr.next();
            }
            System.out.println(); //just a new line
        } while(query.equals("yes")); //while the user wants a new sentence do the above
        System.out.println("Here's another sentence with the subject of your first sentence:");
        sentenceAct = sentence2(fSubject); //calls sentenceAct method
        paragraph(sentenceSub, sentenceAct, fSubject); //calls paragraph method
    }
    public static String getAdj(int randomA){ //returns a random adjective
        String adjective = "";
        switch(randomA){
            case 0: adjective += "gorgeous";
                    break;
            case 1: adjective += "stupid";
                    break;
            case 2: adjective += "silly";
                    break;
            case 3: adjective += "clever";
                    break;
            case 4: adjective += "energetic";
                    break;
            case 5: adjective += "sly";
                    break;
            case 6: adjective += "smart";
                    break;
            case 7: adjective += "huge";
                    break;
            case 8: adjective += "small";
                    break;
            case 9: adjective += "skinny";
                    break;
        }
        return adjective;
    }
    public static String getSub(int randomB){ //returns a random subject
        String subject = "";
        switch(randomB){
            case 0: subject += "dog";
                    break;
            case 1: subject += "cow";
                    break;
            case 2: subject += "platypus";
                    break;
            case 3: subject += "giraffe";
                    break;
            case 4: subject += "camel";
                    break;
            case 5: subject += "baby";
                    break;
            case 6: subject += "cat";
                    break;
            case 7: subject += "squirrel";
                    break;
            case 8: subject += "donkey";
                    break;
            case 9: subject += "dragon";
                    break;
        }
        return subject;
    }
    public static String getVerb(int randomC){ //returns a random verb
        String verb = "";
        switch(randomC){
            case 0: verb += "ran past";
                    break;
            case 1: verb += "jumped over";
                    break;
            case 2: verb += "wrote on";
                    break;
            case 3: verb += "played with";
                    break;
            case 4: verb += "punched";
                    break;
            case 5: verb += "tackled";
                    break;
            case 6: verb += "bit";
                    break;
            case 7: verb += "licked";
                    break;
            case 8: verb += "pooped on";
                    break;
            case 9: verb += "hugged";
                    break;
        }
        return verb;
    }
    public static String getObject(int randomD){ //returns a random object
        String object = "";
        switch(randomD){
            case 0: object += "baby";
                    break;
            case 1: object += "dead body";
                    break;
            case 2: object += "dying person";
                    break;
            case 3: object += "wall";
                    break;
            case 4: object += "teacher";
                    break;
            case 5: object += "child";
                    break;
            case 6: object += "car";
                    break;
            case 7: object += "tree";
                    break;
            case 8: object += "singer";
                    break;
            case 9: object += "moon";
                    break;
        }
        return object;
    }
    public static String sentence(String fAdjective, String fSubject, String fVerb, String fObject){ //assembles a sentence using the above 4 String methods
        String fSentence = "The " + fAdjective + " " + fSubject + " " + fVerb + " the " + fObject + ".";
        System.out.println(fSentence); 
        return fSentence;
    }
    public static String sentence2(String fsubject){
        Random random = new Random();
        String adj2 = ""; //string for adjective of action verb
        String verb2 = ""; //string for action verb
        String obj2 = ""; //string for object of action sentence
        String adj3 = ""; //string for adjective of object
        String actionSent = ""; //string for action sentence
        String timeWords = ""; //string for time words 
        int randoE = random.nextInt(10); 
        int randoF = random.nextInt(10); 
        int randoG = random.nextInt(10); 
        int randoH = random.nextInt(10); 
        int randoI = random.nextInt(2); 
        int rando = random.nextInt(4);
        switch(randoE){ //selects a random adjective
            case 0: adj2 = "particularly";
                    break;
            case 1: adj2 = "especially";
                    break;
            case 2: adj2 = "inevitably";
                    break;
            case 3: adj2 = "stupidly";
                    break;
            case 4: adj2 = "energetically";
                    break;
            case 5: adj2 = "intelligently";
                    break;
            case 6: adj2 = "thoroughly";
                    break;
            case 7: adj2 = "cleverly";
                    break;
            case 8: adj2 = "mindlessly";
                    break;
            case 9: adj2 = "lovingly";
                    break;
        }
        switch(randoF){ //selects a random adjective
            case 0: verb2 = "punched";
                    break;
            case 1: verb2 = "kissed";
                    break;
            case 2: verb2 = "licked";
                    break;
            case 3: verb2 = "killed";
                    break;
            case 4: verb2 = "incapacitated";
                    break;
            case 5: verb2 = "dabbed on";
                    break;
            case 6: verb2 = "yelled at";
                    break;
            case 7: verb2 = "smacked the shit out of";
                    break;
            case 8: verb2 = "beat up";
                    break;
            case 9: verb2 = "laughed at";
                    break;
        }
        switch(randoG){ //selects a random adjective
            case 0: adj3 = "poor";
                    break;
            case 1: adj3 = "wounded";
                    break;
            case 2: adj3 = "handicapped";
                    break;
            case 3: adj3 = "disease-ridden";
                    break;
            case 4: adj3 = "broken";
                    break;
            case 5: adj3 = "dirty";
                    break;
            case 6: adj3 = "small";
                    break;
            case 7: adj3 = "fat";
                    break;
            case 8: adj3 = "anti-social";
                    break;
            case 9: adj3 = "drunk";
                    break;
        }
        switch(randoH){ //selects a random object
            case 0: obj2 = "dog";
                    break;
            case 1: obj2 = "celebrity";
                    break;
            case 2: obj2 = "flying shark";
                    break;
            case 3: obj2 = "flying car";
                    break;
            case 4: obj2 = "B2 Stealth Bomber";
                    break;
            case 5: obj2 = "fighter jet";
                    break;
            case 6: obj2 = "infant";
                    break;
            case 7: obj2 = "person";
                    break;
            case 8: obj2 = "protestor";
                    break;
            case 9: obj2 = "party-goer";
                    break;
        }
        switch(randoI){ //randomizes use of "it" or the actual subject
            case 0: actionSent = "the " + fsubject + " ";
                    break;
            case 1: actionSent += "it ";
                    break;
        }
        switch(rando){ //randomizes time words
            case 0: timeWords = "After that";
                    break;
            case 1: timeWords = "Then";
                    break;
            case 2: timeWords = "Afterward";
                    break;
            case 3: timeWords = "Subsequently";
                    break;
        }
        actionSent = timeWords + ", " + actionSent + adj2 + " " + verb2 + " a random " + adj3 + " " + obj2 + "."; //assembles an action sentence and stores it
        System.out.println(actionSent); //prints out the action sentence
        return actionSent; //returns action sentence to main method
    }
    public static void conclusion(String fSubject){ //method for concluding sentence
        Random random = new Random();
        String conVerb = ""; //string for verb in concluding sentence
        String conNoun = ""; //string for noun in concluding sentence
        int r1 = random.nextInt(10);
        int r2 = random.nextInt(10);
        switch(r1){ //randomizes the conclusion sentence's verb
            case 0: conVerb += "knew about";
                    break;
            case 1: conVerb += "loved";
                    break;
            case 2: conVerb += "enjoyed";
                    break;
            case 3: conVerb += "cherished";
                    break;
            case 4: conVerb += "planned";
                    break;
            case 5: conVerb += "craved";
                    break;
            case 6: conVerb += "performed";
                    break;
            case 7: conVerb += "hated";
                    break;
            case 8: conVerb += "mastered";
                    break;
            case 9: conVerb += "ruined";
                    break;
        }
        switch(r2){ //randomizes the concluding sentence's noun
            case 0: conNoun += "Nae Nae";
                    break;
            case 1: conNoun += "act of brutal murder";
                    break;
            case 2: conNoun += "rapper";
                    break;
            case 3: conNoun += "suicide";
                    break;
            case 4: conNoun += "hit and run";
                    break;
            case 5: conNoun += "war of attrition";
                    break;
            case 6: conNoun += "concrete";
                    break;
            case 7: conNoun += "endangered species";
                    break;
            case 8: conNoun += "world";
                    break;
            case 9: conNoun += "Life of Pi";
                    break;
        }
        System.out.println("Wow, that " + fSubject + " " + conVerb + " the " + conNoun + "."); //assembles the concluding sentence
    }
    public static void paragraph(String sentenceSub, String sentenceAct, String fSubject){ //method to assemble a paragraph
        Random random = new Random();
        int randomJ = random.nextInt(6); 
        System.out.println();
        System.out.println("Here's a paragraph including those two sentences and more:");
        System.out.println();
        System.out.println(sentenceSub); //prints subject sentence
        System.out.println(sentenceAct); //prints the first action sentence generated
        for(int i = 0; i < randomJ ; i++){ //prints a random number of action sentences
            sentence2(fSubject);
        }
        conclusion(fSubject); //prints the concluding sentence
    }
}