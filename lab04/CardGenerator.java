//Hansen Lukman
//9-19-18
//CSE 02 Lab 4 - Random card selector

public class CardGenerator{
  public static void main(String args[]){
    String suit; //initializes a variable for suit on the card
    String number; //initializes a variable for the number on the card
    int num = (int)(Math.random()*52)+1; //random number generator from 1 to 52 inclusive
    suit = ""; //declares the variable of suit as a string
    number = ""; //declares the variable of number as a string
    if(num <= 13){ //if random number generated is less than or equal to 13
      suit = "Diamonds"; //set diamonds as the suit
      num %= 13; //gets the number of the card
    }
    else if(num <= 26){ //if random number generated is less than or equal to 26
      suit = "Clubs"; //set clubs as the suit
      num %= 13; //gets the number of the card
    }
  
    else if(num <= 39){ //if random number generated is less than or equal to 39
      suit = "Hearts"; //set hearts as the suit
      num %= 13; //gets the number of the card
    }
    else if(num <= 52){ //if random number generated is less than or equal to 52
      suit = "Spades"; //set spades as the suit
      num %= 13; //gets the number of the card
    }
    switch (num) {
            case 1:  number = "Ace"; //if the number is 1, set the number of the card to ace
                     break;
            case 2:  number = "2"; //if the number is 2, set the number of the card to 2
                     break;
            case 3:  number = "3"; //if the number is 3, set the number of the card to 3
                     break;
            case 4:  number = "4"; //if the number is 4, set the number of the card to 4
                     break;
            case 5:  number = "5"; //if the number is 5, set the number of the card to 5
                     break;
            case 6:  number = "6"; //if the number is 6, set the number of the card to 6
                     break;
            case 7:  number = "7"; //if the number is 7, set the number of the card to 7
                     break;
            case 8:  number = "8"; //if the number is 8, set the number of the card to 8
                     break;
            case 9:  number = "9"; //if the number is 9, set the number of the card to 9
                     break;
            case 10: number = "10"; //if the number is 10, set the number of the card to 10
                     break;
            case 11: number = "Jack"; //if the number is 11, set the number of the card to jack
                     break;
            case 12: number = "Queen"; //if the number is 12, set the number of the card to queen
                     break;
            case 0: number = "King"; //if the number is 13(in this case the modulus remainder is 0), set the number of the card to king
                     break;
        }
    
    System.out.println("You picked the " + number + " of " + suit); //prints the result of the random card picked
  }
}