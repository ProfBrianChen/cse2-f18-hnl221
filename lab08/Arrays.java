//Hansen Lukman
//11-7-2018
//CSE 02 - 210: Program will generate 100 random numbers within 0 to 99 and print out the instances of each number generated
import java.util.Random;

public class MyProgram
{
    public static void main(String[] args)
    {
        Random rand = new Random();
        int[] numbers = new int[100];
        int[] checker = new int[100];
        int counter = 0;
        for(int i = 0; i < 100; i++){
            numbers[i] = rand.nextInt(100);
        }
        for(int i = 0; i < numbers.length; i++){
            counter = numbers[i];
            checker[counter] += 1;
        }
        for(int i = 0; i < checker.length; i++){
            if(checker[i] > 0){
                System.out.println(i + " occurs " + checker[i] + " time(s).");
            }
        }
    }
}