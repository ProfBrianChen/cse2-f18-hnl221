public class Arithmetic{
  public static void main(String[] args){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    double costPants = pantsPrice * ((double)numPants); //Cost of pants (total, before tax)
    double costShirts = shirtPrice * ((double)numShirts); //Cost of sweatshirts (total, before tax)
    double costBelts = beltCost * ((double)numBelts); //Cost of belts (total, before tax)
    
    double taxPants = costPants * paSalesTax; //Tax charged for purchasing the pants
    double taxShirts = costShirts * paSalesTax; //Tax charged for purchasing the shirts
    double taxBelts = costBelts * paSalesTax; //Tax charged for purchasing the pants
    
    taxPants = ((int)(taxPants * 100)) / 100.0; //Limits tax on pants value to two decimal places
    taxShirts = ((int)(taxShirts * 100)) / 100.0; //Limits tax on sweatshirts value to two decimal places
    taxBelts = ((int)(taxBelts * 100)) / 100.0; //Limits tax on belts value to two decimal places
    
    double subTotal = costPants + costShirts + costBelts; //Cost of Purchase before tax is added
    double taxTotal = taxPants + taxShirts + taxBelts; //Total tax charged for the purchase
    double finalTotal = subTotal + taxTotal; //Total cost of purchase
    
    System.out.println("Total cost for pants: $" + costPants); //Prints total cost for pants
    System.out.println("Total cost for sweatshirts: $" + costShirts); //Prints total cost for sweatshirts
    System.out.println("Total cost for belts: $" + costBelts); //Prints total cost for sweatshirts
    
    System.out.println("Tax on pants: $" + taxPants); //Prints tax charged from purchasing pants
    System.out.println("Tax on sweatshirts: $" + taxShirts); //Prints tax charged from purchasing sweatshirts
    System.out.println("Tax on belts: $" + taxBelts); //Prints tax charged from purchasing belts
    
    System.out.println("Subtotal: $" + subTotal); //Prints total before taxes are added
    System.out.println("Tax total: $" + taxTotal); //Prints total of taxes of each item purchased
    System.out.println("Final total: $" + finalTotal); //Prints the total of entire purchase after taxes
    
  }
}