//Hansen Lukman
//11-12-2018
//CSE 02 - 210: Program will generate and shuffle a deck of cards. It will also draw a hand from the shuffled deck and print the hand 
//              until the user does not want a new hand. When the deck runs out of cards, a new deck will be generated.
import java.util.Scanner;
import java.util.Random;
public class Shuffling{ 
    public static void main(String[] args) { 
        Scanner scan = new Scanner(System.in); 
        //suits club, heart, spade or diamond 
        String[] suitNames={"C","H","S","D"};    
        String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
        String[] cards = new String[52]; 
        String[] hand = new String[5]; 
        int numCards = 5; 
        int again = 1; 
        int index = 51;
        for (int i=0; i<52; i++){ //generates a full deck of 52 cards
            cards[i]=rankNames[i%13]+suitNames[i/13]; 
            System.out.print(cards[i]+" "); 
        } 
        System.out.println();
        shuffle(cards); //shuffles the deck generated
        System.out.println();
        System.out.println("Shuffled: ");
        printArray(cards,numCards); //prints shuffled deck of cards
        while(again == 1){ //while the user wants a new hand
            if((index + 1) >= numCards){ //if the amount of cards left is greater or equal to the number of cards in a hand, draw the cards, and ask user if they want a new hand
                System.out.println();
                hand = getHand(cards,index,numCards); 
                System.out.println("Hand: ");
                printArray(hand,numCards);
                System.out.println();
                index -= numCards;
                System.out.println("Enter a 1 if you want another hand drawn"); 
                again = scan.nextInt(); 
            } else {  //if the amount of cards left is less than the number of cards in a hand, generate a new deck and draw the cards, and ask user if they want a new hand
                System.out.println();
                index = 51;
                shuffle(cards);
                System.out.println("Ran out of cards... Here's a new deck: ");
                printArray(cards,numCards);
                System.out.println();
                hand = getHand(cards,index,numCards); 
                System.out.println("Hand: ");
                printArray(hand,numCards);
                System.out.println();
                index -= numCards;
                System.out.println("Enter a 1 if you want another hand drawn");
                again = scan.nextInt();
            }
        }
    }
    public static void printArray(String[] cards, int numCards){ //Prints either the hand drawn or the deck generated, depending on the parameters entered
        if(cards.length > 5){ //this is for printing the deck
            for(int i = 0; i < 52; i++){
                System.out.print(cards[i] + " ");
            }
        } else { //this is for printing a hand
            for(int i = 0; i < numCards; i ++){
                System.out.print(cards[i] + " ");
            }
        }
        System.out.println();
    }
    public static String[] shuffle(String[] cards){ //shuffles the deck of cards array
        Random num = new Random();
        String holder1;
        String holder2;
        int spot1;
        int spot2;
        for(int i = 0; i < 52; i++){
            spot1 = num.nextInt(52);
            spot2 = num.nextInt(52);
            holder1 = "";
            holder2 = "";
            holder1 = cards[spot1];
            holder2 = cards[spot2];
            cards[spot1] = holder2;
            cards[spot2] = holder1;
        }
        return cards;
    }
    public static String[] getHand(String[] cards, int index, int numCards){ //draws a hand from the "top" of the deck of cards (String[] cards)
        String[] hand = new String[5];
        for(int i = 0; i < numCards; i++){
            hand[i] = cards[index];
            index--;
        }
        return hand;
    }
}