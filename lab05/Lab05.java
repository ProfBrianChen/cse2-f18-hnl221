//Hansen Lukman
//10-8-2018
//CSE02 - 210 
//Lab 05 - Asks user to input course number, department name, number of meetings per week, the name of the instructor, and the number of enrolled students
//Also checks if each input is a string or integer based on the prompt and infinitely loops until the user inputs the correct type (string or int)

import java.util.Scanner;

public class Lab05{
  public static void main(String args[]){
    Scanner scnr = new Scanner(System.in);
    int courseNum;
    int numMeetings;
    int numStudents = 0;
    String junk = "";
    String departName = "";
    String nameInstructor = "";
    
    System.out.print("Please enter the Course Number: "); //Prompts user for course number input.
    while(!scnr.hasNextInt()){ //while user input is not an integer
        junk = scnr.nextLine(); //removes user input from "conveyor belt"
        System.out.print("Please enter the course number again as an integer: "); //re-prompts user for course number input.
    }
    courseNum = scnr.nextInt(); //stores user provided integer for course number
    junk = scnr.nextLine(); //clears "conveyor belt"
    
    System.out.print("Please enter the Department Name: "); //Prompts user for Department Name input
    while(scnr.hasNextLine()){ //while there is any user input
        junk = scnr.nextLine(); //clears "converyor belt" but also stores user input
        if(junk.length() > 0){ //checks if user input is a string
            break; //breaks out of while loop if it is a string
        } else { //otherwise,
            System.out.print("Please enter the Department Name again as a String: ");//re-prompt user to enter a string.
        }
    }
    departName = junk; //stores user provided string as the department name
    
    System.out.print("Please enter the number of times the class meets each week: "); //prompts user to enter the number of class meetings per week
    while(!scnr.hasNextInt()){ //while user input is not an integer
        junk = scnr.nextLine(); //removes user input from "conveyor belt"
        System.out.print("Please enter the the number of times the class meets each week again as an integer: "); //re-prompts user to enter the number of class meetings per week
    }
    numMeetings = scnr.nextInt(); //stores user provided integer for number of meetings
    junk = scnr.nextLine(); //clears "conveyor belt"
    
    System.out.print("Please enter the Instructor's Name: "); //Prompts user for the Instructor's Name
    while(scnr.hasNextLine()){ //while there is any user input
        junk = scnr.nextLine(); //clears "converyor belt" but also stores user input
        if(junk.length() > 0){ //checks if user input is a string
            break; //breaks out of while loop if it is a string
        } else { //otherwise,
            System.out.print("Please enter the Instructor's Name again as a String: ");//re-prompt user to enter a string.
        }
    }
    nameInstructor = junk; //stores user provided string as the Instructor's Name
    
    System.out.print("Please enter the Number of Students enrolled in the course: "); //prompts user to enter the number of class meetings per week
    while(!scnr.hasNextInt()){ //while user input is not an integer
        junk = scnr.nextLine(); //removes user input from "conveyor belt"
        System.out.print("Please enter the the Number of Students enrolled again as an integer: "); //re-prompts user to enter the number of class meetings per week
    }
    numStudents = scnr.nextInt(); //stores user provided integer for number of meetings
    junk = scnr.nextLine(); //clears "conveyor belt"
    
    System.out.println(""); //Just adding a space after inputs
    System.out.println("Course Number: " + courseNum); //prints course number
    System.out.println("Department Name: " + departName); //prints department name
    System.out.println("Number of Meetings per week: " + numMeetings); //prints number of meetings
    System.out.println("Instructor Name: " + nameInstructor); //prints name of instructor
    System.out.println("Number of Enrolled Students: " + numStudents); //prints number of enrolled students
  }
}
