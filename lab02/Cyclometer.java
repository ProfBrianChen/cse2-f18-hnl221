//Hansen Lukman
//9/5/2018
//CSE2 - lab02 
//Program: My bicycle cyclometer records two kinds of data, the time elapsed in seconds, and the number of rotations of the front wheel during that time
public class Cyclometer{
  //main method required for every Java program
  public static void main(String[] args){
    //input data
    int secsTrip1 = 480; //Duration in seconds of Trip 1
    int secsTrip2 = 3220; //Duration in seconds of Trip 2
    int countsTrip1 = 1561; //number of rotations Trip 1
    int countsTrip2 = 9037; //number of rotations Trip 2
    
    //useful constants and conversions
    double wheelDiameter = 27.0, //Diameter of wheel
    PI = 3.14159, //value of pi
    feetPerMile = 5280, //conversion for feet into miles
    inchesPerFoot = 12,//amount of inches in one foot
    secondsPerMinute = 60; //amount of seconds in a minute
    
    double distanceTrip1, distanceTrip2, totalDistance; //initializing distance variables for each trip and total distance
    
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
    //Above prints out duration for trip 1 and the amount of counts
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
    //Above prints out duration for trip 2 and the amount of counts
    
    //calculations
    distanceTrip1 = countsTrip1 * wheelDiameter * PI; 
    //Above gives distance in inches 
    //For each count, a rotation of the wheel travels the diameter (in inches) times PI
    distanceTrip1 /= inchesPerFoot * feetPerMile; //Gives distance in miles
    //The following line combines the two steps above into one line
    distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
    totalDistance = distanceTrip1 + distanceTrip2; //This computes the total distance travelled
    
    //Prints out the output data
    System.out.println("Trip 1 was " + distanceTrip1 + " miles."); //prints out trip one distance travelled in miles
    System.out.println("Trip 2 was " + distanceTrip2 + " miles."); //prints out trip two distance travelled in miles
    System.out.println("The total distance was " + totalDistance + " miles"); //prints out total distance travelled in miles
    
  }//end of main method
}//end of class