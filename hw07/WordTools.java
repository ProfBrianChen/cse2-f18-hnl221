//Hansen Lukman
//11-3-2018
//CSE 02 - 210: Program will take a user string (paragraph), output a menu 
//              and the user will input a menu option. The program will perform
//              perform the task under the chosen menu option.
import java.util.Scanner;
public class WordTools
{
    public static void main(String[] args)
    {
        char input = 'a'; 
        String input1 = "";
        String text = "";
        Scanner sc = new Scanner(System.in);
        text = sampleText(sc, text);
        do{ //the program will,
            input1 = printMenu(); //print the menu and store user input for menu option
            String word = "";
            int flip = 0;
            while(flip == 0){ //loops while this condition is met (this is for infinitely looping while user input is invalid)
                switch(input1){ //checks what the user wants
                    case "c": input = input1.charAt(0);
                              System.out.println();
                              System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(text));
                              flip++;
                              break;
                    case "w": input = input1.charAt(0);
                              System.out.println();
                              System.out.println("Number of words: " + getNumOfWords(text));
                              flip++;
                              break;
                    case "f": input = input1.charAt(0);
                              System.out.println();
                              System.out.print("Enter a word or phrase to be found: ");
                              word = sc.nextLine();
                              System.out.println("\"" + word + "\" instances: " + findText(text, word));
                              flip++;
                              break;
                    case "r": input = input1.charAt(0);
                              text = replaceExclamation(text);
                              System.out.println("Edited text: " + text);
                              flip++;
                              break;
                    case "s": input = input1.charAt(0);
                              text = shortenSpace(text);
                              System.out.println("Edited text: " + text);
                              flip++;
                              break;
                    case "q": input = input1.charAt(0);
                              flip++;
                              break;
                    default: System.out.println("You entered an invalid input -- try again.");
                             input1 = printMenu();
                             input = input1.charAt(0);
                             break;
                }
            }
        } while(input != 'q'); //if the user entered q, exit the loop
        System.out.println();
        System.out.println("Goodbye.");
    }
    
    public static String sampleText(Scanner sc, String text) //method for prompting user to input a text string, returning the text string
    {
        System.out.println("Enter a sample text:");
        text += sc.nextLine();
        System.out.println();
        System.out.println("You entered: " + text);
        return text;
    }
    
    public static String printMenu() //prints the menu and returns user input for chosen menu item
    {
        Scanner sc = new Scanner(System.in);
        String input = "";
        System.out.println();
        System.out.println("MENU");
        System.out.println("c - Number of non-whitespace characters");
        System.out.println("w - Number of words");
        System.out.println("f - Find text");
        System.out.println("r - Replace all !'s");
        System.out.println("s - Shorten spaces");
        System.out.println("q - Quit");
        System.out.println();
        System.out.print("Choose an option: ");
        input = sc.next();
        return input;
    }
    public static int getNumOfNonWSCharacters(String text){ //returns number of non white-space characters in the user text string
        int counter = 0;
        int characters = 0;
        while(counter < text.length()){ //loop to check every character in the user input
            if(!Character.isWhitespace(text.charAt(counter))){ //if the character is not a white-space
                characters++; //add 1 to the count of characters
            }
            counter++;
        }
        return characters;
    }
    public static int getNumOfWords(String text){ //returns number of words in the user text string
        int counter = 0;
        int numWords = 0;
        while(counter < text.length()){ //loops to "scan" every character until the end of the text string is reached
            int flip = 0; //0 if not on a word
            while(!Character.isWhitespace(text.charAt(counter))){ //loops while "scanner" is on a word(whitespace indicates end of word)
                counter++; //move on to the next character in the word
                flip++; //indicates that "scanner" is on a word
                if(counter == text.length()){
                    break;
                }
            }
            if(flip == 0){ //if a word was not found
                counter++; //move on to the next character in the text
            } else { //if a word was found
                numWords++; //add 1 to the word count
            }
        }
        return numWords;
    }
    public static int findText(String text, String word){ //returns how many times a phrase or word appears in user text
        Scanner sc = new Scanner(System.in);
        int counter = 0; 
        int wordCount = 0;
        while((counter + 1) <= text.length()){ //loops until the end of the text is reached
            int counter2 = 0;
            while(text.charAt(counter) == word.charAt(counter2)){ //loops while each character matches the word/phrase user is searching for
                counter2++;
                counter++;
                if(counter2 == word.length()){ //if the word/phrase matches
                    wordCount++; //add 1 to the count
                    break;
                }
            }
            counter++;
        }
        return wordCount;
    }
    public static String replaceExclamation(String text){ //replaces all exclamation points in the text string with periods and returns the edited text
        text = text.replace('!', '.');
        return text;
    }
    public static String shortenSpace(String text){ //replaces any spaces that are more than two with one space
        int counter = 0;
        int startIndex = 0;
        int endIndex;
        int end = text.length();
        String onePart = "";
        String removedPart = "";
        String otherPart = "";
        for(int i = 0; i < end; i++){ //loops until the end of text is reached but the text will change so this is not reliable
            int spaceCounter = 0; //counter for the number of spaces
            int flip = 0;
            if(counter >= text.length()){ //this checks if at the end of the text after changes are made, this is reliable
                break; //exit loop
            }
            while(Character.isWhitespace(text.charAt(counter))){ //if theres a white space, this checks how many spaces there are
                if(flip == 0){ //this allows for storing the index of the first space, so after storing once, it wont go on to the next slot and store that
                    startIndex = counter; //stores index of first space
                    flip++;
                }
                spaceCounter++;
                counter++;
                if(counter >= text.length()){ //exits loop if at the end of text
                    break;
                }
            }
            endIndex = counter; //stores index of first character after space
            if(spaceCounter > 1){ //if there is more than one space consecutively
                onePart = text.substring(0, startIndex); //stores a substring of the text before the space sequence that is about to be edited
                removedPart = text.substring(startIndex, endIndex); //stores the spaces substring about to be removed
                otherPart = text.substring(endIndex, text.length()); //stores the substring of the text after the space sequence that is about to be edited
                counter -= removedPart.length(); //removes length of the spaces to account for the edit in the counter
                text = onePart + " " + otherPart; //joins the two parts of the string with only one space in between rather than more than one
            }
            counter++;
        }
        return text;
    }
}