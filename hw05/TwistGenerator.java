//Hansen Lukman
//10-11-2018
//CSE02-210: program will take user input (and make sure it is a positive integer) and create a twist output
import java.util.Scanner;
public class TwistGenerator {
    public static void main(String args[]){
        Scanner scnr = new Scanner(System.in);
        
        int length; //Integer for length of the twist
        int counter = 0; //Integer for counting length of twist when building it
        String junk = ""; //String for clearing "conveyor belt"
        String topRow = ""; //String for top row of twist
        String midRow = ""; //String for mid row of twist
        String botRow = ""; //String for bot row of twist
        
        System.out.print("Please enter a positive integer for length of your twist: ");//prompts user to enter a positive integer
        while(!scnr.hasNextInt() || (length = scnr.nextInt()) <= 0){//while the user did not provide an integer or provided a negative integer. 
                                                                    //Also stores user input as length if it is an integer and removes 
                                                                    //the user entered integer from the "conveyor belt".
            
            junk = scnr.nextLine(); //removes user input from "conveyor belt" if it is not an integer
            System.out.print("Try again. Please enter a positive Integer for the length: "); //re-prompts user for a positive integer
        }
        while(counter < length){//while the twist is not the desired length
            counter++; //add 1 to counter to calculate position in the twist
            
            if((counter % 3) == 1){ //if the position is one of three in the twist pattern
                topRow += "\\"; //add a "\" to the top row
            } else if((counter % 3) == 2){ //if the position is the two of three in the twist pattern
                topRow += " "; // add a space to the top row
            } else { //else if the position is three of three in the twist pattern
                topRow += "/"; //add "/" to the top row
            }
            
            if((counter % 3) == 1){ //if the position is one of three in the twist pattern
                midRow += " "; //add a space to the middle row
            } else if((counter % 3) == 2){//if the position is two of three in the twist pattern
                midRow += "X"; //add an "X" to the middle row
            } else { //if the position is three of three in the twist pattern
                midRow += " "; //add a space to the middle row
            }
            
            if((counter % 3) == 1){ //if the position is one of three in the twist pattern
                botRow += "/"; //add a "/" to the bottom row
            } else if((counter % 3) == 2){ //if the position is two of three in the twist pattern
                botRow += " "; //add a space to the bottom row
            } else { //if the position is three of three in the twist pattern
                botRow += "\\"; //add "\" to the bottom row
            }
        }
        
        System.out.println(""); //just adding a space to separate twist from inputs
        System.out.println(topRow);//prints out top row of twist
        System.out.println(midRow);//prints out mid row of twist
        System.out.println(botRow);//prints out bot row of twist
    }
}
