 //Hansen Lukman
 //Due Sept. 04, 2018
 //CSE02 - Section 210: WelcomeClass Program
public class WelcomeClass {
  /* This will print out:
  -----------
  | WELCOME |
  -----------
  ^  ^  ^  ^  ^  ^
 / \/ \/ \/ \/ \/ \
<-E--J--K--0--0--0->
 \ /\ /\ /\ /\ /\ /
  v  v  v  v  v  v   */

  public static void main(String args[]){
    System.out.println("  -----------"); //First Line 
    System.out.println("  | WELCOME |"); //Second Line
    System.out.println("  -----------"); //Third Line
    System.out.println("  ^  ^  ^  ^  ^  ^"); //Fourth Line
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\"); //Fifth Line
    System.out.println("<-H--N--L--2--2--1->"); //Sixth Line
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /"); //Seventh Line
    System.out.println("  v  v  v  v  v  v"); //Eighth Line
    System.out.println("  I am from Indonesia."); //Ninth Line
    }
  }