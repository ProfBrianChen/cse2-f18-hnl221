// Hansen Lukman, Matt Solomon, Philip Stefanov
// CSE2 Venmo Variables: 9/3/2018

public class VenmoVariables{
  public static void main(String args[]){
    double currBal = 5000.00; //Current account balance.
    double amtWD = 100.00; //Amount withdrawn from Venmo to Bank Account.
    double amtSent = 10.00; //Sending ten dollars.
    double balAfterWD = currBal - amtWD; //Amount of money after withdrawal from Venmo into bank account.
    double venmoBalAfterSent = currBal - amtSent; //Amount of money left in Venmo account after sending to someone.
    System.out.println("Current Balance: " + currBal); //Prints current balance.
    System.out.println("Amount of Withdrawl: " + amtWD); //Prints amount of withdrawl.
    System.out.println("Amount sent to recipient: " + amtSent); //Prints amount of money sent to recipient.
    System.out.println("Balance after withdrawl: " + balAfterWD); //Prints balance after withdrawl.(excludes sending)
    System.out.println("Balance in Venmo account after sending to recipient: " + venmoBalAfterSent); //Prints balance after sending money to recipient.(excludes withdrawl)
  }
}