//Hansen Lukman
//11-20-2018
//CSE 02 - 210: Program will generate an array of 10 random integers from 0-9, remove a 
//		specified index from that array, and remove a specific number completely off the array
import java.util.Scanner;
import java.util.Random;
public class RemoveElements{
     public static void main(String [] arg){
    	Scanner scan=new Scanner(System.in);
        int num[]=new int[10];
        int newArray1[];
        int newArray2[];
        int index,target;
    	String answer="";
    	do{
          	System.out.println("Random input 10 ints [0-9]");
          	num = randomInput();
          	String out = "The original array is:";
          	out += listArray(num);
          	System.out.println(out);
          	System.out.println();
         
          	System.out.print("Enter the index: ");
          	index = scan.nextInt();
          	newArray1 = delete(num,index);
          	String out1="The output array is ";
          	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
          	System.out.println(out1);
          	System.out.println();
         
            System.out.print("Enter the target value ");
          	target = scan.nextInt();
          	newArray2 = remove(num,target);
          	String out2="The output array is ";
          	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
          	System.out.println(out2);
          	System.out.println();
          	 
          	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
          	answer=scan.next();
    	}while(answer.equals("Y") || answer.equals("y"));
    }
    //Method generates an array that will be printed out
    public static String listArray(int num[]){
    	String out="{";
    	for(int j=0;j<num.length;j++){
      	    if(j>0){
        	    out+=", ";
      	    }
      	    out+=num[j];
    	}
    	out+="} ";
    	return out;
    }
    //Method generates an array of 10 random integers each within 0-9 and returns that array.
    public static int[] randomInput(){
        Random num = new Random();
        int[] list = new int[10];
        for(int i = 0; i < list.length; i++){
            list[i] = num.nextInt(10);
        }
        return list;
    }
    //Method removes the number in the chosen index and returns an updated array.
    public static int[] delete(int[] list, int pos){
        Scanner scan = new Scanner(System.in);
        int[] array;
        if(pos > (list.length - 1) || pos < 0){
            System.out.println("Error -- out of bounds of array");
            array = new int[10];
        } else {
            array = new int[9];
        }
        int incrementer = 0;
        for(int i = 0; i < list.length; i++){
            if(i == pos){
            } else {
                array[incrementer] = list[i];
                incrementer++;
            }
        }
        return array;
    }
    //Method removes from the array every number equal to the number of the user input.
    public static int[] remove(int[] list, int target){
        int counter = 0;
        int counter2 = 0;
        int length = list.length;
        int[] array = new int[length];
        for(int i = 0; i < list.length; i++){ //this for loop gets the length of the updated array after removal of numbers and finds the index of the numbers
            if(list[i] == target){ //this will save the index of each chosen number found 
                array[counter] = i;
                counter+=1;
                length--;
            }
        }
        if(length == list.length){ //if nothing was removed, state element was not found.
            System.out.println("Element " + target + " was not found.");
            array[0] = -1;
        } else { //else state that it was found.
            System.out.println("Element " + target + " was found.");
        }
        int[] updatedArray = new int[length]; //initializes the array with the calculated length.
        int counter3 = 0;
        for(int j = 0; j < list.length; j++){ //for loop assigns integers to the indexes of the updated array.(Gets rid of the chosen number)
            if(array[counter2] == j){//if on an index with the chosen number
                counter2++; //move on to the next index with the chosen number and do nothing
            } else if(counter3 == length){ //breaks out of loop if the updated array is full
                break;
            } else { //else copy the number from the original array to the updated array.
                updatedArray[counter3] = list[j];
                counter3++;
            }
        }
        return updatedArray;
    }
}