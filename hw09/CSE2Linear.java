//Hansen Lukman
//11-20-2018
//CSE 02 - 210: Program will prompt user to enter 15 grades in ascending order,
//		look for a grade using binary search, then scramble, and look for a different grade with 
//		linear search
import java.util.Scanner;
import java.util.Random;
public class CSE2Linear
{
    public static void main(String[] args)
    {
        int num[] = new int[15];
        int indicator = 0;
        indicator = numbers(num, indicator);
        if(indicator == 0){
            for(int j = 0; j < num.length; j++){
                System.out.print(num[j]+ " ");
            }
            System.out.println();
            binarySearch(num);
            num = scrambler(num);
            System.out.println("Scrambled:");
            for(int k = 0; k < num.length; k++){
                System.out.print(num[k]+ " ");
            }
            System.out.println();
            linearSearch(num);
        }
    }
    //This method prompts the user to enter 15 ascending grades and checks for correct user input
    public static int numbers(int[] num, int indicator)
    {
        Scanner scnr = new Scanner(System.in);
        System.out.println("Enter 15 ascending ints for final grades in CSE2:");
        for(int i = 0; i < num.length; i++){
            if(!scnr.hasNextInt()){
                System.out.println("Error -- Non-integer Entered");
                indicator++;
                break;
            } else if(num[i] > 100 || num[i] < 0){
                System.out.println("Error -- " + num[i] + " is not within 0-100.");
                indicator++;
                break;
            } else {
                num[i] = scnr.nextInt();
            }
            if(i > 0 && num[i - 1] > num[i]){
                System.out.println("Error -- " + num[i] + " is not ascending.");
                indicator++;
                break;
            }
        }
        return indicator;
    }
    //this method will conduct binary search on the grades entered
    public static void binarySearch(int[] num){
        Scanner scnr = new Scanner(System.in);
        int high = num.length - 1;
        int mid = 0;
        int low = 0;
        int i = 0;
        System.out.print("Enter a grade to search for: ");
        while(!scnr.hasNextInt()){
            String junk = scnr.nextLine();
            System.out.println("Error -- Invalid input, please enter an integer: ");
        }
        int numSearch = scnr.nextInt();
        while (high >= low) {
            i += 1;
            mid = (high + low) / 2;
            if (num[mid] < numSearch) {//if the number at the middle index is lower than the number looking for,
                low = mid + 1; 	       //set the lower bound at the index above the middle index
            } else if (num[mid] > numSearch) { //if the number at mid index is higher than the number looking for,
                high = mid - 1;		       //set the higher bound at the index below the middle index
            } else { //once there are no more matches or the number is found, break
                mid = num[mid];	
                break;
            }
        }
        if(mid == numSearch){ //prints out if number was found and in how many iterations
            System.out.println(numSearch + " was found in " + i + " iterations.");
        } else {
            System.out.println(numSearch + " was not found in " + i + " iterations.");
        }
    }
    //method scrambles the grades up
    public static int[] scrambler(int[] nums){ 
        Random num = new Random();
        int holder1;
        int holder2;
        int spot1;
        int spot2;
        for(int i = 0; i < 15; i++){
            spot1 = num.nextInt(15);
            spot2 = num.nextInt(15);
            holder1 = nums[spot1];
            holder2 = nums[spot2];
            nums[spot1] = holder2;
            nums[spot2] = holder1;
        }
        return nums;
    }
    //this method will perform linear search on the scrambled grades list
    public static void linearSearch(int[] num){
        Scanner scnr = new Scanner(System.in);
        System.out.println("Enter a grade to search for: ");
        while(!scnr.hasNextInt()){ //checks if user input is an integer
            String junk = scnr.nextLine();
            System.out.println("Error -- Invalid input, please enter an integer: ");
        }
        int numSearch = scnr.nextInt();
        int counter = 0;
        int flip = 0;
        for(int i = 0; i < num.length; i++){ //checks every individual index for the number
            counter++;
            if(num[i] == numSearch){
                flip++;
                break;
            }
        }
        if(flip != 0){ //if number wasn't found, indicate so along with number of iterations
            System.out.println(numSearch + " was not found in " + counter + " iterations.");
        } else { //if number was found, indicate so along with number of iterations
            System.out.println(numSearch + " was found in " + counter + " iterations.");
        }
    }
}