//Hansen Lukman
//9-25-18
//CSE 02 - Dice roller (random or user provided) using if/else-if
import java.util.Scanner;

public class CrapsIf{
  public static void main(String args[]){
    Scanner myScanner = new Scanner(System.in); //declares instance of the Scanner object and calls the Scanner constructor
    
    int dice1; //initializes an integer for one dice
    int dice2; //initializes an integer for the other dice
    String diceCast;
    char diceCastChar;
    
    System.out.print("Would you like to randomly cast dice? Yes (y) or No (n): "); //asks user if they want to randomly cast dice
    diceCast = myScanner.next(); //stores user input from above line
    diceCastChar = diceCast.charAt(0);
    
    if(diceCastChar == 'y'){ //if user said they want to randomize
      dice1 = (int)(Math.random()*6)+1; //roll a random number from 1 to 6 for dice 1
      dice2 = (int)(Math.random()*6)+1; //roll a random number from 1 to 6 for dice 2
      System.out.println("Dice 1: " + dice1); //prints out what dice 1 rolled
      System.out.println("Dice 2: " + dice2); //prints out what dice 2 rolled
    } else { //if user said no
      
      System.out.print("Choose a value (from 1 to 6) for one dice: "); //prompts user to enter a number from 1 to 6 for dice 1
      dice1 = myScanner.nextInt(); //stores input for the value of dice 1
      
      if (dice1 < 1 || dice1 > 6){ //if user input is not from 1 to 6 for dice 1
        System.out.print("Please try again (Enter a value from 1 to 6): "); //prompts user to re-enter a number from 1 to 6 for dice 1
        dice1 = myScanner.nextInt(); //stores input from above line
      }
      
      System.out.print("Choose a value (from 1 to 6) for the other dice: "); //prompts user to enter a number from 1 to 6 for dice 2
      dice2 = myScanner.nextInt(); //stores input for the value of dice 2 
      
      if (dice2 < 1 || dice2 > 6){ //if user input is not from 1 to 6 for dice 2
        System.out.print("Please try again (Enter a value from 1 to 6): "); //prompts user to re-enter a number from 1 to 6 for dice 2
        dice2 = myScanner.nextInt(); //stores input from above line
      }
    }
    if (dice1 == 1 && dice2 == 1){ //if rolled 1,1
      System.out.println("You rolled: Snake Eyes!"); //prints out result
    } else if ((dice1 == 1 && dice2 == 2) || (dice1 == 2 && dice2 == 1)){ //if rolled 1,2 or 2,1
      System.out.println("You rolled: Ace Deuce!"); // prints out result
    } else if (dice1 == 2 && dice2 == 2){ //if rolled 2,2
      System.out.println("You rolled: Hard Four!"); //prints out result
    } else if ((dice1 == 1 && dice2 == 3) || (dice1 == 3 && dice2 == 1)){ //if rolled 1,3 or 3,1
      System.out.println("You rolled: Easy Four!"); //prints out result
    } else if ((dice1 == 3 && dice2 == 2) || (dice1 == 2 && dice2 == 3)){ //if rolled 2,3 or 3,2
      System.out.println("You rolled: Fever Five!"); //prints out result
    } else if (dice1 == 3 && dice2 == 3){ //if rolled 3,3
      System.out.println("You rolled: Hard Six!"); //prints out result
    } else if ((dice1 == 1 && dice2 == 4) || (dice1 == 4 && dice2 == 1)){ //if rolled 1,4 or 4,1
      System.out.println("You rolled: Fever Five!"); //prints out result
    } else if ((dice1 == 4 && dice2 == 2) || (dice1 == 2 && dice2 == 4)){ //if rolled 2,4 or 4,2
      System.out.println("You rolled: Easy Six!"); //prints out result
    } else if ((dice1 == 3 && dice2 == 4) || (dice1 == 4 && dice2 == 3)){ //if rolled 3,4 or 4,3
      System.out.println("You rolled: Seven Out!"); //prints out result
    } else if (dice1 == 4 && dice2 == 4){ //if rolled 4,4
      System.out.println("You rolled: Hard Eight!"); //prints out result
    } else if ((dice1 == 1 && dice2 == 5) || (dice1 == 5 && dice2 == 1)){ //if rolled 1,5 or 5,1
      System.out.println("You rolled: Easy Six!"); //prints out result
    } else if ((dice1 == 5 && dice2 == 2) || (dice1 == 2 && dice2 == 5)){ //if rolled 2,5 or 5,2 
      System.out.println("You rolled: Seven Out"); //prints out result
    } else if ((dice1 == 3 && dice2 == 5) || (dice1 == 5 && dice2 == 3)){ //if rolled 3,5 or 5,3
      System.out.println("You rolled: Easy Eight!"); //prints out result
    } else if ((dice1 == 4 && dice2 == 5) || (dice1 == 5 && dice2 == 4)){ //if rolled 4,5 or 5,4
      System.out.println("You rolled: Nine!"); //prints out result
    } else if (dice1 == 5 && dice2 == 5){ //if rolled 5,5
      System.out.println("You rolled: Hard Ten!"); //prints out result
    } else if ((dice1 == 1 && dice2 == 6) || (dice1 == 6 && dice2 == 1)){ //if rolled 1,6 or 6,1
      System.out.println("You rolled: Seven Out!"); //prints out result
    } else if ((dice1 == 6 && dice2 == 2) || (dice1 == 2 && dice2 == 6)){ //if rolled 2,6 or 6,2
      System.out.println("You rolled: Easy Eight!"); //prints out result
    } else if ((dice1 == 6 && dice2 == 3) || (dice1 == 3 && dice2 == 6)){ //if rolled 3,6 or 6,3
      System.out.println("You rolled: Nine!"); //prints out result
    } else if ((dice1 == 4 && dice2 == 6) || (dice1 == 6 && dice2 == 4)){ //if rolled 4,6 or 6,4
      System.out.println("You rolled: Easy Ten!");//prints out result
    } else if ((dice1 == 5 && dice2 == 6) || (dice1 == 6 && dice2 == 5)){ //if rolled 5,6 or 6,5
      System.out.println("You rolled: Yo-Leven!"); //prints out result
    } else if (dice1 == 6 && dice2 == 6){ //if rolled 6,6
      System.out.println("You rolled: Boxcars!"); //prints out result
    } else {
      System.out.println("You still didn't choose a number between 1 and 6 inclusive for one or both of the dice even after I asked you repeatedly. Learn to read.");
    }
  }
}