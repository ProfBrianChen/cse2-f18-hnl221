//Hansen Lukman
//9-25-18
//CSE 02 - Dice roller (random or user provided) using switches
import java.util.Scanner;

public class CrapsSwitch{
  public static void main(String args[]){
    Scanner myScanner = new Scanner(System.in); //declares instance of the Scanner object and calls the Scanner constructor
    
    int dice1 = 0; //initializes an integer for one dice
    int dice2 = 0; //initializes an integer for the other dice
    String diceCast; //initializes dice cast input String
    char diceCastChar; //initializes dice cast char conversion
    
    System.out.print("Would you like to randomly cast dice? Yes (y) or No (n): "); //asks user if they want to randomly cast dice
    diceCast = myScanner.next(); //stores user input from above line
    diceCastChar = diceCast.charAt(0); //converts user input from a String to a char
    
    switch (diceCastChar){
      case 'y': dice1 = (int)(Math.random()*6)+1; //roll a random number from 1 to 6 for dice 1
                dice2 = (int)(Math.random()*6)+1; //roll a random number from 1 to 6 for dice 2
                break;
      case 'n': System.out.print("Choose a value (from 1 to 6) for one dice: "); //prompts user to enter a number from 1 to 6 for dice 1
                dice1 = myScanner.nextInt(); //stores input for the value of dice 1
                
                switch (dice1){
                  case 1: break; //if user input is 1, do nothing
                  case 2: break; //if user input is 2, do nothing
                  case 3: break; //if user input is 3, do nothing
                  case 4: break; //if user input is 4, do nothing                 
                  case 5: break; //if user input is 5, do nothing
                  case 6: break; //if user input is 6, do nothing
                  default: //if user input does not lie within 1 to 6 inclusive                             
                    System.out.print("Please try again (Enter a value from 1 to 6): "); //prompts user to re-enter a number from 1 to 6
                    dice1 = myScanner.nextInt(); //stores input from above line
                    break;
                }
                
                System.out.print("Choose a value (from 1 to 6) for the other dice: "); //prompts user to enter a number from 1 to 6 for dice 2
                dice2 = myScanner.nextInt(); //stores input for the value of dice 2 
      
                switch (dice2){
                  case 1: break; //if user input is 1, do nothing
                  case 2: break; //if user input is 2, do nothing
                  case 3: break; //if user input is 3, do nothing
                  case 4: break; //if user input is 4, do nothing                 
                  case 5: break; //if user input is 5, do nothing
                  case 6: break; //if user input is 1, do nothing
                  default:  //if user input does not lie within 1 to 6 inclusive                            
                    System.out.print("Please try again (Enter a value from 1 to 6): "); //prompts user to re-enter a number from 1 to 6
                    dice2 = myScanner.nextInt(); //stores input from above line
                    break;
                }
                break;
      default: System.out.println("Invalid input, please re-run program and answer with (y) or (n)."); //prints out result if user did not input y or n correctly
               break;
    }
    switch(dice1){
          case 1: //if the first dice rolled 1
            switch(dice2){ 
              case 1: System.out.println("You rolled: Snake Eyes!"); //prints out result if rolled 1,1 
                      break;
              case 2: System.out.println("You rolled: Ace Deuce!"); // prints out result if rolled 1,2
                      break;
              case 3: System.out.println("You rolled: Easy Four!"); //prints out result if rolled 1,3
                      break;
              case 4: System.out.println("You rolled: Fever Five!"); //prints out result if rolled 1,4
                      break;
              case 5: System.out.println("You rolled: Easy Six!"); //prints out result if rolled 1,5
                      break;
              case 6: System.out.println("You rolled: Seven Out"); //prints out result if rolled 1,6
                      break;
              default: System.out.println("You done goofed, re-run the program."); //prints out result if directions were not followed
                       break;
            }
          break;
          case 2: //if the first dice rolled 2
            switch(dice2){
              case 1: System.out.println("You rolled: Ace Deuce!"); // prints out result if rolled 2,1
                      break;
              case 2: System.out.println("You rolled: Hard Four!"); //prints out result if rolled 2,2
                      break;
              case 3: System.out.println("You rolled: Fever Five!"); //prints out result if rolled 2,3
                      break;
              case 4: System.out.println("You rolled: Easy Six!"); //prints out result if rolled 2,4
                      break;
              case 5: System.out.println("You rolled: Seven Out"); //prints out result if rolled 2,5
                      break;
              case 6: System.out.println("You rolled: Easy Eight"); //prints out result if rolled 2,6
                      break;
              default: System.out.println("You done goofed, re-run the program."); //prints out result if directions were not followed
                      break;
            }
          break;
          case 3: //if the first dice rolled 3
            switch(dice2){
              case 1: System.out.println("You rolled: Easy Four!"); //prints out result if rolled 3,1
                      break;
              case 2: System.out.println("You rolled: Fever Five!"); //prints out result if rolled 3,2
                      break;
              case 3: System.out.println("You rolled: Hard Six!"); //prints out result if rolled 3,3
                      break;
              case 4: System.out.println("You rolled: Seven Out!"); //prints out result if rolled 3,4
                      break;
              case 5: System.out.println("You rolled: Easy Eight!"); //prints out result if rolled 3,5
                      break;
              case 6: System.out.println("You rolled: Nine"); //prints out result if rolled 3,6
                      break;
              default: System.out.println("You done goofed, re-run the program."); //prints out result if directions were not followed
                       break;
            }
          break;
          case 4: //if the first dice rolled 4
            switch(dice2){
              case 1: System.out.println("You rolled: Fever Five!"); //prints out result if rolled 4,1
                      break;
              case 2: System.out.println("You rolled: Easy Six!"); //prints out result if rolled 4,2
                      break;
              case 3: System.out.println("You rolled: Seven Out!"); //prints out result if rolled 4,3
                      break;
              case 4: System.out.println("You rolled: Hard Eight!"); //prints out result if rolled 4,4
                      break;
              case 5: System.out.println("You rolled: Nine!"); //prints out result if rolled 4,5
                      break;
              case 6: System.out.println("You rolled: Easy Ten"); //prints out result if rolled 4,6
                      break;
              default: System.out.println("You done goofed, re-run the program."); //prints out result if directions were not followed
                       break;
            }
          break;
          case 5: //if first dice rolled 5
            switch(dice2){
              case 1: System.out.println("You rolled: Easy Six!"); //prints out result if rolled 5,1
                      break;
              case 2: System.out.println("You rolled: Seven Out"); //prints out result if rolled 5,2
                      break;
              case 3: System.out.println("You rolled: Easy Eight!"); //prints out result if rolled 5,3
                      break;
              case 4: System.out.println("You rolled: Nine!"); //prints out result if rolled 5,4
                      break;
              case 5: System.out.println("You rolled: Hard Ten"); //prints out result if rolled 5,5
                      break;
              case 6: System.out.println("You rolled: Yo-Leven"); //prints out result if rolled 5,6
                      break;
              default: System.out.println("You done goofed, re-run the program."); //prints out result if directions were not followed
                       break;
            }
          break;
          case 6: //if first dice rolled 6
            switch(dice2){
              case 1: System.out.println("You rolled: Seven Out"); //prints out result if rolled 6,1
                      break;
              case 2: System.out.println("You rolled: Easy Eight"); //prints out result if rolled 6,2
                      break;
              case 3: System.out.println("You rolled: Nine"); //prints out result if rolled 6,3
                      break;
              case 4: System.out.println("You rolled: Easy Ten");//prints out result if rolled 6,4
                      break;
              case 5: System.out.println("You rolled: Yo-Leven"); //prints out result if rolled 6,5
                      break;
              case 6: System.out.println("You rolled: Boxcars"); //prints out result if rolled 6,6
                      break;
              default: System.out.println("You done goofed, re-run the program."); //prints out result if directions were not followed
                       break;
            }
          break;
      default: System.out.println("You done goofed, re-run the program."); //prints out result if directions were not followed
               break;
    }
  }
}