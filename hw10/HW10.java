//Hansen Lukman
//12-4-2018
//CSE 02 - 210: Program will generate a game of tic-tac-toe until there is a winner or a draw.
import java.util.Scanner;
public class HW10
{
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        int[][] gameBoard = {{1,2,3},{4,5,6},{7,8,9}}; //2D array for game board
        int[] holder = {0,0,0}; //array for storing {whose turn, number chosen, number of turns played}
        int endGame = 0;
        getGameBoard(gameBoard);
        do{ 
            getNum(holder,gameBoard);
            System.out.println();
            updateBoard(holder, gameBoard);
            endGame = checkWinner(holder, gameBoard, endGame);
            holder[2]++; //increments number of turns played
            if(holder[2] == 9 && endGame == 0){ //if completed 9 turns without a winner, game is a draw
                System.out.println("Game over -- It's a Draw!");
                break;
            }
        } while(endGame == 0); //plays the game until there is a winner.
    }
    public static void getGameBoard(int[][] gameBoard){ //method to print out the unaltered game board
        for(int row = 0; row < gameBoard.length; row++){
            for(int column = 0; column < gameBoard[row].length; column++){
                System.out.print(gameBoard[row][column]);
                System.out.print("      	");
            }
            System.out.println();
        }
    }
    public static void getNum(int[] holder, int[][] gameBoard){ //method to prompt for user input and call the method checker
        Scanner sc = new Scanner(System.in);
        if(holder[0] == 0){ //if it's O's turn, prompt for O's move, check, and change the turn
            System.out.print("O's turn, choose a spot(corresponding to number): ");
            checker(holder, gameBoard);
            holder[0]+=1; //indicate it's now X's turn
        } else if(holder[0] != 0){ //if it's X's turn, prompt for X's move, check, and change the turn
            System.out.print("X's turn, choose a spot(corresponding to number): ");
            checker(holder, gameBoard);
            holder[0]-=1; //indicates it's now O's turn
        }
    }
    public static void checker(int[] holder, int[][] gameBoard){ //method will check for correct user input and infinitely prompt until correct input is found
        Scanner sc = new Scanner(System.in);
        String junk = "";
        int flip = 0; //variable for condition if correct input is made (1 = correct)
        int flip2 = 0; //variable for condition to print line if incorrect input detected (see next comment)
        while(flip == 0){
            if(flip2 == 1){ //only activates if conditions for correct input were not satisfied
                junk = sc.nextLine();
                System.out.print("Please re-enter an available position on the board: ");
            }
            while(!sc.hasNextInt() || (holder[1] = sc.nextInt()) <  1 || holder[1] > 9){ //checks if input is an integer an within the bounds
                junk = sc.nextLine();
                System.out.print("Invalid input -- please try again (Enter an integer available on the game board): ");
            }
            for(int row = 0; row < gameBoard.length; row++){ //checks if user input was already selected
                for(int column = 0; column < gameBoard[row].length; column++){
                    if(gameBoard[row][column] == holder[1]){
                        if(holder[0] == 0){ //else if it's O's turn
                            gameBoard[row][column] = 0; //indicate this spot on the board is an O
                            flip += 1;
                            break;
                        } else if(holder[0] == 1){ //else if it's X's turn
                            gameBoard[row][column] = 10; //indicate this spot on the board is an X
                            flip += 1;
                            break;
                        }
                    }
                }
            }
            flip2 = 1;
        }
    }
    public static void updateBoard(int[] holder, int[][] gameBoard){ //method will print an updated board including the O's and X's
        for(int row = 0; row < gameBoard.length; row++){ //checks if user input was already selected
            for(int column = 0; column < gameBoard[row].length; column++){
                if(gameBoard[row][column] == 0){ //if indicated O, print out an O
                    System.out.print("O");   
                    System.out.print("      	");
                } else if(gameBoard[row][column] == 10){ //else if indicated X, print out an X
                    System.out.print("X");
                    System.out.print("      	");
                } else {
                    System.out.print(gameBoard[row][column]); //if if neither is indicated, print out original number
                    System.out.print("      	");
                }
            }
            System.out.println();
        }
    }
    public static int checkWinner(int[] holder, int[][] gameBoard, int endGame){ //method for checking for a winner, returns 1 if end of game
        if(((gameBoard[0][0] == gameBoard[0][1]) && (gameBoard[0][0] == gameBoard[0][2])) 
            || ((gameBoard[1][0] == gameBoard[1][1]) && (gameBoard[1][0] == gameBoard[1][2])) 
            || ((gameBoard[2][0] == gameBoard[2][1]) && (gameBoard[2][0] == gameBoard[2][2]))){  //checks if anyone won horizontally
            if(holder[0] == 1){ //if this indicates that it's now X's turn, then O won
                System.out.println("Game over -- O wins!");
                endGame = 1; //indicates end of game
            } else { //else, X won
                System.out.println("Game over -- X wins!");
                endGame = 1; //indicates end of game
            }
        } else if(((gameBoard[0][0] == gameBoard[1][0]) && (gameBoard[0][0] == gameBoard[2][0]))
                    || ((gameBoard[0][1] == gameBoard[1][1]) && (gameBoard[0][1] == gameBoard[2][1]))
                    || ((gameBoard[0][2] == gameBoard[1][2]) && (gameBoard[0][2] == gameBoard[2][2]))){ //checks if anyone won vertically
                    if(holder[0] == 1){ //if this indicates that it's now X's turn, then O won
                        System.out.println("Game over -- O wins!");
                        endGame = 1; //indicates end of game
                    } else { //else, X won
                        System.out.println("Game over -- X wins!");
                        endGame = 1; //indicates end of game
                    } 
        } else if(((gameBoard[0][0] == gameBoard[1][1]) && (gameBoard[0][0] == gameBoard[2][2]))
                    || ((gameBoard[0][2] == gameBoard[1][1]) && (gameBoard[0][2] == gameBoard[2][0]))){
                    if(holder[0] == 1){ //if this indicates that it's now X's turn, then O won
                        System.out.println("Game over -- O wins!");
                        endGame = 1; //indicates end of game
                    } else { //else, X won
                        System.out.println("Game over -- X wins!");
                        endGame = 1; //indicates end of game
                    }   
        }
        return endGame;
    }
}