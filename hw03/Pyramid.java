//Hansen Lukman
//9-18-2018
//CSE 02 Homework 03 - Calculator for Volume of a Pyramid
import java.util.Scanner;
public class Pyramid{
  public static void main(String args[]){
    Scanner myScanner = new Scanner(System.in); //declares instance of the Scanner object and calls the Scanner constructor
    
    System.out.print("The square side of the pyramid is (input length): "); //prompts user for the length of one side of the square base of the pyramid
    double length = myScanner.nextDouble(); //stores the input of the side length of the base as "length"
    double areaBase = length * length; //calculates and stores the area of the square base as "areaBase"
    
    System.out.print("The height of the pyramid is (input height): "); //prompts user for the height of the pyramid
    double height = myScanner.nextDouble(); //stores the input of the height of the pyramid as "height"
    
    double volumePyramid = (areaBase * height) / 3; //calculates volume of pyramid based on inputs
    System.out.println("The volume inside the pyramid is: " + volumePyramid); //prints out resultant volume of pyramid
  }
}