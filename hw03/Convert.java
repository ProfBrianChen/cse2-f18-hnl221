//Hansen Lukman
//9-18-2018
//CSE 02 Homework 03 - Program will convert quantity of rain from inches to cubic miles
import java.util.Scanner;

public class Convert{
  public static void main(String args[]){
    Scanner myScanner = new Scanner(System.in); //declares instance of the Scanner object and calls the Scanner constructor
    
    System.out.print("Enter the affected area in acres: "); //prompts user to enter affected area in acres
    double affectedAcres = myScanner.nextDouble(); //stores the entered value for the affected area
    
    System.out.print("Enter the rainfall in the affected area: "); //prompts user to enter the amount of rainfall in inches
    double rainFall = myScanner.nextDouble(); //stores the entered value of rainfall
    rainFall /= 12; //converts rainfall to feet
    
    double acreFoot = affectedAcres * rainFall; //amount of rainfall in acre-feet
    double cubicFeet = acreFoot * 43560; //amount of rainfall in cubic feet
    double cubicMiles = cubicFeet * 6.79357e-12; //amount of rainfall in cubic miles
    
    System.out.println(cubicMiles + " cubic miles"); //prints result of volume of rain calculation in cubic miles
  }
}