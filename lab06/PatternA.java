//Hansen Lukman
//10-10-2018
//CSE02 - 210: Program will print a pyramid pattern for PatternA example
import java.util.Scanner;
public class PatternA
{
    public static void main(String[] args)
    {
        Scanner scnr = new Scanner(System.in);
        int input; //stores user input for number of rows
        int counter; //keeps track of which row the loop is on
        int outNum = 1; //outNum is the number that will be added to the current output string
        String output = ""; //the output for the current row
        String junk = "";
        
        System.out.print("How many rows in your pyramid? (Enter an integer from 1 to 10): ");
        while(!scnr.hasNextInt() || (input = scnr.nextInt()) < 1 || input > 10){ //if user input is not an integer and stores the user input and checks if the input is within 1-10
            junk = scnr.nextLine(); //clears the "conveyor belt"
            System.out.print("Please try again. Enter an Integer from 1 to 10: ");//prompts user to re-enter an integer
        }
        
        for(int i = 0;i < input; i++){ //loop that will print out "input" number of rows
            if(i == 0){ //if on the first row
                System.out.println(outNum + " "); //print out a 1
                output = output + outNum + " "; //adds 1 to the string output
                outNum++; //increments outNum for the next number addition to string output
            } else { //if past the first row
                output = output + outNum + " "; //add the next number to the string output
                System.out.println(output); //print out the new string output
                outNum++; //increment outNum for the next number addition to string output
            }
        }
    }
}
