//Hansen Lukman
//10-10-2018
//CSE02 - 210
//Lab06: Program will print a pyramid pattern for PatternB example

import java.util.Scanner;
public class PatternD
{
    public static void main(String[] args)
    {
        Scanner scnr = new Scanner(System.in);
        int input; //stores user input for number of rows
        int counter = 0; //just a counter for various loops
        int outNum; //outNum is the number that will be added to the current output string
        int length; //stores length of output
        String output = ""; //the output for the row
        String junk = "";
        
        System.out.print("How many rows in your pyramid? (Enter an integer from 1 to 10): ");
        while(!scnr.hasNextInt() || (input = scnr.nextInt()) < 1 || input > 10){ //if user input is not an integer and stores the user input and checks if the input is within 1-10
            junk = scnr.nextLine(); //clears the "conveyor belt"
            System.out.print("Please try again. Enter an Integer from 1 to 10: ");//prompts user to re-enter an integer
        }
        
        outNum = input; //sets the first number in the row to input
        
        if(outNum == 10){ //checks if first number is a 10 (accounts for extra string slot)
            output += 10; //sets first number in string as 10
            outNum--; //decrements to next number
            counter++; //increments counter 
        } else { //if not, then theres no need to account for the extra string slot
            output += input; //stores first number in output string
            outNum--; //decrements to next number
            counter++; //increments counter
        }
        
        while(counter < input){ //loops "input" number of times (this creates the first row)
            output = output + " " + outNum; //add the decremented number to the string
            outNum--; //decrement to next number
            counter++; //increments counter
        }
        System.out.println(output + " "); //prints out first row
        
        for(int i = 1;i < input; i++){ //loop that will print out "input" number of rows
            length = output.length(); //updates current output length
            output = output.substring(2, length); //removes the first number and space from the string
            System.out.println(output + " "); //prints out the new line after removal
        }
    }
}
