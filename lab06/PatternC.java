//Hansen Lukman
//10-10-2018
//CSE02 - 210
//Lab06: Program will print a pyramid pattern for PatternC example

import java.util.Scanner;
public class PatternC
{
    public static void main(String[] args)
    {
        Scanner scnr = new Scanner(System.in);
        int input; //stores user input for number of rows
        int counter = 0; //just a counter for various loops
        int outNum; //outNum is the number that will be added to the current output string
        int remover; //integer to remove a part of the string output
        String outputNum = ""; //the output for the numbers
        String outputSpaces = "                             "; //the output for the spaces
        String wholeLine = "";
        String junk = "";
        
        System.out.print("How many rows in your pyramid? (Enter an integer from 1 to 10): ");
        while(!scnr.hasNextInt() || (input = scnr.nextInt()) < 1 || input > 10){ //if user input is not an integer and stores the user input and checks if the input is within 1-10
            junk = scnr.nextLine(); //clears the "conveyor belt"
            System.out.print("Please try again. Enter an Integer from 1 to 10: ");//prompts user to re-enter an integer
        }
        
        while(counter <  input){ //this loop will execute input times
            outputSpaces += " "; //add a space to string outputSpaces
            counter++; //increment counter
        }
        
        if(input == 10){ //1st part in accounting for extra string slot for "10"
            outputSpaces += " "; //add an extra space
        }
            
        counter = 0; //resets counter
        
        for(int i = 0;i < input; i++){ //loop that will print out "input" number of rows
            counter++; //increments counter
            remover = outputSpaces.length(); //sets remover equal to the length of string output
            outputSpaces = outputSpaces.substring(0, remover - 1); //removes the last space from the string outputSpaces
            if(i == 9){ //2nd part accounting for extra string slot for "10", removes extra space
                outputSpaces = outputSpaces.substring(0, remover - 2); //removes last two spaces from the string outputSpaces
            }
            outputNum = counter + outputNum; //adds an incremented number to the string outputNum
            wholeLine = outputSpaces + outputNum; //combines the spaces with the numbers to create row
            System.out.println(wholeLine); //prints out row
        }
    }
}