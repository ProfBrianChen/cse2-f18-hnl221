//Hansen Lukman
//10-10-2018
//CSE02 - 210
//Lab06: Program will print a pyramid pattern for PatternB example

import java.util.Scanner;
public class PatternB
{
    public static void main(String[] args)
    {
        Scanner scnr = new Scanner(System.in);
        int input; //stores user input for number of rows
        int counter; //keeps track of which row the loop is on
        int outNum = 2; //outNum is the number that will be added to the current output string
        int remover; //integer to remove a part of the string output
        String output = "1 "; //the output 
        String junk = "";
        
        System.out.print("How many rows in your pyramid? (Enter an integer from 1 to 10): ");
        while(!scnr.hasNextInt() || (input = scnr.nextInt()) < 1 || input > 10){ //if user input is not an integer and stores the user input and checks if the input is within 1-10
            junk = scnr.nextLine(); //clears the "conveyor belt"
            System.out.print("Please try again. Enter an Integer from 1 to 10: ");//prompts user to re-enter an integer
        }
        
        while(outNum <= input){ //while the next number is less than or equal to user input
            output = output + outNum + " "; //add the incremented number to the string
            outNum++; //increment to next number
        }
        System.out.println(output); //prints out first line
        
        for(int i = 1;i < input; i++){ //loop that will print out "input" number of rows
            if(input == 10 && i == 1){
                remover = output.length(); //sets remover equal to the length of string output
                output = output.substring(0, (remover - 3)); //removes the last number from the string
                System.out.println(output); //prints out the new line after removal
            } else {
                remover = output.length(); //sets remover equal to the length of string output
                output = output.substring(0, (remover - 2)); //removes the last number from the string
                System.out.println(output); //prints out the new line after removal
            }
        }
    }
}