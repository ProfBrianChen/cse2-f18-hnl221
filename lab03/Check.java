//Hansen Lukman
//9-12-2018
//CSE02 Lab 03
//Program will calculate the amount each person will have to pay when splitting a check
import java.util.Scanner;

public class Check {
  public static void main(String args[]){ // main method required for every Java program
    Scanner myScanner = new Scanner(System.in); //declares instance of the Scanner object and calls the Scanner constructor
    
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //Prompts the user for the original cost of the check
    double checkCost = myScanner.nextDouble(); //accepts and stores the user input of the original cost of the check
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //prompts user for the percentage of tip the user wants to pay
    double tipPercent = myScanner.nextDouble(); //accepts and stores the user input of the percentage of tip the user wants to pay
    tipPercent /= 100; //converts tip percentage into a decimal value
    
    System.out.print("Enter the number of people who went out to dinner: "); //prompts user to enter the number of people attending dinner
    int numPeople = myScanner.nextInt(); //accepts and stores the user input of how many people are attending dinner
    
    double totalCost; //initializes a double for the total cost of the check
    double costPerPerson; //initializes a double for the cost of dinner for each person after splitting the check
    int dollars, //whole dollar amount of cost
          dimes, pennies; //for storing digits to the right of the decimal point for the cost
    totalCost = checkCost * (1 + tipPercent); //calculates the total cost of the check before splitting
    costPerPerson = totalCost / numPeople; //calculates the amount each person will have to pay after splitting
    dollars = (int)costPerPerson; //gets whole amount, dropping decimal fraction
    dimes = (int)(costPerPerson * 10) % 10; //gets dimes amount (0.X0)
    pennies = (int)(costPerPerson * 100) % 10; //gets pennies amount (0.0X)
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes+pennies); //prints out amount each person will have to pay
    
  } //end of main method
} // end of class

